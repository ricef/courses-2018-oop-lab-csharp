﻿using System;

namespace Properties {

    public class Card
    {
        private readonly string seed;
        private readonly string name;
        private readonly int ordial;

        public Card(string name, string seed, int ordial)
        {
            this.name = name;
            this.ordial = ordial;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        // TODO improve
        public string GetSeed()
        {
            return seed;
        }

        // TODO improve
        public string GetName()
        {
            return name;
        }

        // TODO improve
        public int GetOrdinal()
        {
            return ordial;
        }

        public override string ToString()
        {
            // TODO understand string interpolation
            return $"{GetType().Name}(Name={GetName()}, Seed={GetSeed()}, Ordinal={GetOrdinal()})";
        }

        public override bool Equals(object obj)
        {
            // TODO improve
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            // TODO improve
            return base.GetHashCode();
        }
    }

}